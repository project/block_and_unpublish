Block and unpublish.

This module's function is simple. Allows you to block users and unpublish all of their nodes and comments in a single action.

All affects of this module can be reverted while the module remains installed.
If you've 

Warning: Users and content that have been blocked/unpublished with this module will remain so once uninstalled.

----- How to use ------
It couldn't be simpler.

- Navigate to the "People" page.
- Select the user(s) that you want to apply this action to.
- Select the "Block user and unpublish content & comments" option from the "Update options" select list.
- Apply.

To undo just apply the "Undo: Block user and unpublish content & comments" update to user(s).

If you are using the admin_views module for your user list you will need to add the operations to your view. (Go to the "Administration: Users" view and edit the "Operations: User" field).

----- Permissions -----
Users require permission to use this module.

If using admin_views (or any other module that relies on VBO to display the user actions) you will need to have the actions_permissions module enabled and set these permissions under the "Actions permissions (VBO)" group on the permissions page.
